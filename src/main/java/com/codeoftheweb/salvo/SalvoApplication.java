package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@SpringBootApplication
public class SalvoApplication extends SpringBootServletInitializer {



	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	@Bean
	public CommandLineRunner initData(PlayerRepository playerrepository, GameRepository gamerepository, GamePlayerRepository gameplayerrepository, ShipRepository shiprepository, SalvoRepository salvorepository, ScoreRepository scorerepository) {
		return (args) -> {


			Date date = new Date();
			Date date2 = Date.from(date.toInstant().plusSeconds(3600));
			Date date3 = Date.from(date2.toInstant().plusSeconds(3600));



			Game g1 = new Game(date);
			gamerepository.save(g1);


			Player p2 = new Player("a@a",passwordEncoder().encode("123"));
			playerrepository.save(p2);
			GamePlayer gameplayer2 = new GamePlayer(p2,g1,date);
			gameplayerrepository.save(gameplayer2);
			List<String> salvoplayer2turn1 = new ArrayList<String>();
			salvoplayer2turn1.add("A1");
			salvoplayer2turn1.add("A2");
			salvoplayer2turn1.add("D4");
			salvoplayer2turn1.add("D5");
			salvoplayer2turn1.add("D6");
			Salvo salvo2 = new Salvo(gameplayer2,1,salvoplayer2turn1);
			salvorepository.save(salvo2);


			Player p3 = new Player("alguien@alguien","123");
			playerrepository.save(p3);
			Game g2 = new Game(date2);
			gamerepository.save(g2);
			GamePlayer gameplayer3 = new GamePlayer(p3,g2,date);
			gameplayerrepository.save(gameplayer3);


			Score puntuacion3 = new Score(g1,p2,(float)0.5,date);
			scorerepository.save(puntuacion3);

			List<String> posbarco1 = new ArrayList<String>();
			posbarco1.add("A1");
			posbarco1.add("A2");

			List<String> posbarco2 = new ArrayList<String>();
			posbarco2.add("A7");
			posbarco2.add("B7");

			List<String> posbarco3 = new ArrayList<String>();
			posbarco3.add("E6");
			posbarco3.add("E7");
			posbarco3.add("E8");

			List<String> posbarco4 = new ArrayList<String>();
			posbarco4.add("F6");
			posbarco4.add("F7");
			posbarco4.add("F8");



			Ship barco3 = new Ship("destroyer", gameplayer2, posbarco3);
			Ship barco4 = new Ship("submarine", gameplayer2, posbarco4);
			shiprepository.save(barco3);
			shiprepository.save(barco4);

			Player p4 = new Player("t.almeida@ctu.gov",passwordEncoder().encode("mole"));
			playerrepository.save(p4);
			GamePlayer gameplayer4 = new GamePlayer(p4,g2,date);
			gameplayerrepository.save(gameplayer4);

			Player p5 = new Player("j.bauer@ctu.gov",passwordEncoder().encode("24"));
			playerrepository.save(p5);



		};
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}
}


	@Configuration
	class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

		@Autowired
		PlayerRepository playerRepository;

		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(inputName-> {
				Player player = playerRepository.findByEmail(inputName);
				if (player != null) {
					return new User(player.getEmail(), player.getPassword(),
							AuthorityUtils.createAuthorityList("USER"));
				} else {
					throw new UsernameNotFoundException("Unknown user: " + inputName);
				}
			});
		}

	}

	@EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {

			http.authorizeRequests()
					.antMatchers( "/web/games_3.html").permitAll()
					.antMatchers( "/web/**").permitAll()
					.antMatchers( "/api/games.").permitAll()
					.antMatchers( "/api/players").permitAll()
					.antMatchers( "/api/game_view/*").hasAuthority("USER")
					.antMatchers( "/rest/*").denyAll()
					.anyRequest().permitAll();



			http.formLogin().usernameParameter("username").passwordParameter("password")

					.loginPage("/api/login").permitAll();


					http.logout().logoutUrl("/api/logout");

		http.csrf().disable();

			// if user is not authenticated, just send an authentication failure response
			http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

			// if login is successful, just clear the flags asking for authentication
			http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

			// if login fails, just send an authentication failure response
			http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

			// if logout is successful, just send a success response
			http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());

		}

		private void clearAuthenticationAttributes(HttpServletRequest request) {
			HttpSession session = request.getSession(false);
			if (session != null) {
				session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
			}
		}

	}







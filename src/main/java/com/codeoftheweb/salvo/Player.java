package com.codeoftheweb.salvo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

/**
 * Clase player, con ID, EMAIL, Username,Password
 */
@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String email;
    private String username;
    private String password;

    @OneToMany(mappedBy="playerid", fetch=FetchType.EAGER)
    private Set<GamePlayer> gamePlayers;

    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    private Set<Score> scores;

    public Player() {}
    public Player(String email, String password) {
        this.username = email;
        this.email = email;
        this.password = password;

    }

    public Set<Score> getScores() {
        return scores;
    }

    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username){
        this.username = username;

    }

    public void addGamePlayer(GamePlayer gameplayer) {
        gameplayer.setPlayerid(this);
        gamePlayers.add(gameplayer);
    }

    public float getScore(Player player){

        return (float) (getWin(player) * 1 + getTied(player) * 0.5 + getLost(player) * 0);
    }

    public int getWin(Player player){

        return (int) player.getScores().stream().filter(a -> a.getScore() == 1).count();
    }

    public int getLost(Player player){

        return (int) player.getScores().stream().filter(a -> a.getScore() == 0).count();
    }

    public int getTied(Player player){

        return (int) player.getScores().stream().filter(a -> a.getScore() == 0.5).count();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public List<Game> getGames() {
        return gamePlayers.stream().map(sub2 -> sub2.getGameid()).collect(toList());
    }

}


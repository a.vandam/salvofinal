package com.codeoftheweb.salvo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api")
public class SalvoController {


    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GamePlayerRepository gameplayerrepository;

    @Autowired
    private PlayerRepository playerRepository;

     @Autowired
    private PasswordEncoder passwordEncoder;

     @Autowired
     private ShipRepository shipRepository;

     @Autowired
     private SalvoRepository salvoRepository;

     @Autowired
     private ScoreRepository scoreRepository;
//-------------------------------------------------------------------------------------------------------------//
//registro de jugadores postPlayer
   @RequestMapping(path = "/players", method = RequestMethod.POST)
       public ResponseEntity<Object> postPlayer(@RequestParam String email, @RequestParam String password) {

           if (email.isEmpty()){
               return new ResponseEntity<>("Missing data", HttpStatus.FORBIDDEN);
           }

        Player player = playerRepository.findByEmail(email);
        if (player !=  null) {
            return new ResponseEntity<>("Name already in use", HttpStatus.FORBIDDEN);
        }

       playerRepository.save(new Player(email, passwordEncoder.encode(password)));
        return new ResponseEntity<>( "name add",HttpStatus.CREATED);
    }


//------------------------------------------------------------------------------------------------------------
    //postGames - añade juego

    /**
     *
     * @param authentication Usuario autenticado, si hay, en forma d eobjecto
     * @return devuelve HTTP respondses dependiendo si quiere loguearse o crear un usuario.
     *
     */
    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> postGame(Authentication authentication) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player authenticatedPlayer = getAuthentication(authentication);
        if(authenticatedPlayer == null){
            return new ResponseEntity<>(makeErrorMap("No name given"), HttpStatus.FORBIDDEN);
        } else {
            Date date = Date.from(java.time.ZonedDateTime.now().toInstant());
            Game auxGame = new Game(date);
            gameRepository.save(auxGame);

            GamePlayer auxGameP = new GamePlayer(authenticatedPlayer,auxGame,date);

            gameplayerrepository.save(auxGameP);
            return new ResponseEntity<>(makeMap("gpid", auxGameP.getId()), HttpStatus.CREATED);
        }
    }




    @RequestMapping(path = "/games/{gameid}/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> unirse(@PathVariable Long gameid, Authentication authentication ){

        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player usuariologueado = getAuthentication(authentication);
        Game game = gameRepository.getOne(gameid);


        if (usuariologueado == null) {

            return new ResponseEntity<>(makeMap("error ","tiene que estar logueado"),HttpStatus.UNAUTHORIZED);
        }

        if(gameRepository.findById(gameid) == null){

            return new ResponseEntity<>(makeMap("error ","no existe el juego"),HttpStatus.FORBIDDEN);
        }

        if(game.getGamePlayers().size() == 2 ) {

            return new ResponseEntity<>(makeMap("error ","el juego esta lleno"),HttpStatus.FORBIDDEN);
        }

        GamePlayer gameplayer = new GamePlayer(usuariologueado,game,new Date());
        gameplayerrepository.save(gameplayer);
        return new ResponseEntity<>(makeMap("gpid", gameplayer.getId()),HttpStatus.CREATED);
    }


    @RequestMapping(value = "/games/players/{id}/ships", method = RequestMethod.POST)
    private ResponseEntity<Map<String,Object>> AddShips(@PathVariable long id,
                                                        @RequestBody Set<Ship> ships,
                                                        Authentication authentication) {
        GamePlayer gamePlayer = gameplayerrepository.findById(id).orElse(null);
        Player loggedPlayer = getAuthentication(authentication);
        Player currentuser = gamePlayer.getPlayerid();
        if (loggedPlayer == null)
            return new ResponseEntity<>(makeMap("error", "no player logged in"), HttpStatus.UNAUTHORIZED);
        if (gamePlayer == null)
            return new ResponseEntity<>(makeMap("error", "no such gamePlayer"), HttpStatus.UNAUTHORIZED);


        if(loggedPlayer != currentuser){

            return new ResponseEntity<>(makeMap("error ","no es tu juego"),HttpStatus.UNAUTHORIZED);

    } else {
            if (gamePlayer.getShips().isEmpty()) {

                ships.forEach(ship -> ship.setGamePlayer(gamePlayer));
                gamePlayer.setShips(ships);
                shipRepository.saveAll(ships);
                return new ResponseEntity<>(makeMap("ok", "Ships saved"), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(makeMap("error", "Player already has ships"), HttpStatus.FORBIDDEN);
            }
        }
    }


    @RequestMapping("/games/players/{id}/salvoes")
    private ResponseEntity<Map<String,Object>> AddSalvoes(@PathVariable long id,
                                                          @RequestBody Salvo salvo,
                                                          Authentication authentication) {


        GamePlayer gamePlayer = gameplayerrepository.findById(id).orElse(null);
        Player loggedPlayer = getAuthentication(authentication);

        if (loggedPlayer == null)
            return new ResponseEntity<>(makeMap("error", "No player logged in"), HttpStatus.UNAUTHORIZED);
        if (gamePlayer == null)
            return new ResponseEntity<>(makeMap("error", "No such gamePlayer"), HttpStatus.UNAUTHORIZED);
        if (wrongGamePlayer(gamePlayer, loggedPlayer)) {
            return new ResponseEntity<>(makeMap("error", "Wrong GamePlayer"), HttpStatus.UNAUTHORIZED);
        } else {
            if (!turnos(gamePlayer, salvo)) {
                salvo.setTurn(gamePlayer.getSalvos().size() + 1);
                salvo.setGameplayer(gamePlayer);
                salvoRepository.save(salvo);
                return new ResponseEntity<>(makeMap("ok", "Salvoes added"), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(makeMap("error", "Player has fired salvoes in this turn"), HttpStatus.FORBIDDEN);
            }
        }
    }

    private boolean turnos(GamePlayer gameplayer, Salvo salvo){

             boolean tiene = false;
            for ( Salvo s:gameplayer.getSalvos() ){

                if(s.getTurn() == salvo.getTurn()){
                    tiene = true;
                }
            }
            return tiene;
    }


    private boolean wrongGamePlayer( GamePlayer gamePlayer, Player player){

        boolean corretGP= gamePlayer.getPlayerid().getId() != player.getId();
        return corretGP;
    }


    @RequestMapping(path ="/game_view/{id}" )
    public ResponseEntity<Map<String,Object>> AuthorizeUser(@PathVariable long id,
                                                            Authentication authentication){
        Map<String, Object> dto = new LinkedHashMap<>();
        Player loggedPlayer = getAuthentication(authentication);
        if (loggedPlayer == null) {
            return new ResponseEntity<>(makeMap("error","no player logged in"), HttpStatus.FORBIDDEN);
        }

        GamePlayer gamePlayer = gameplayerrepository.getOne(id);
        if(wrongGamePlayer(gamePlayer,loggedPlayer)){
            return new ResponseEntity<>(makeMap("error","Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        if(id > numberGamePlayers()){
            return new ResponseEntity<>(makeMap("error","no such GamePlayer"), HttpStatus.FORBIDDEN);
        }

        GamePlayer opponent = opponentGamePlayer(gamePlayer);
        if(opponent == null) {

            return new ResponseEntity<>(makeMap("error","Waiting for next player"), HttpStatus.FORBIDDEN);
        }
        dto.put("id", gamePlayer.getGameid().getId());
        dto.put("joinDate", gamePlayer.getGameid().getCreationDate());
        dto.put("gameState",getGameState(gamePlayer,opponent));
        dto.put("gamePlayers", gamePlayer.getGameid().getGamePlayers().stream().map(gamePlayerr -> makeGameplayerDTO2(gamePlayerr)).collect(toList()));

        if (gamePlayer.getShips().isEmpty()){

            dto.put("ships", new ArrayList<>());
        } else {
            dto.put("ships", gamePlayer.getShips().stream().map(ship -> makeShipDTO(ship)).collect(toList()));
        }
        if (opponent == null || gamePlayer.getSalvos().isEmpty()) {
            dto.put("salvoes", new ArrayList<>());
        } else {
            dto.put("salvoes", getSalvoList(gamePlayer.getGameid()));
        }

            dto.put("hits",Hitsdto(gamePlayer,opponent));

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    private Map<String,Object> Hitsdto(GamePlayer self,
                                       GamePlayer opponent){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("self", getHits(self,opponent));
        dto.put("opponent", getHits(opponent,self));
        return dto;
    }

    private Map<String,Object> EmptyHits(){
        Map<String,Object> dto = new LinkedHashMap<>();
        dto.put("self", new ArrayList<>());
        dto.put("opponent", new ArrayList<>());
        return dto;
    }

    private Map<String, Object> makeShipDTO(Ship ship) {

        Map<String,Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id",ship.getId());
        dto.put("locations",ship.getLocations());
        dto.put("type",ship.getType());

        return dto;

    }

    private Map<String, Object> makeGameplayerDTO2(GamePlayer gamePlayers) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("gpid", gamePlayers.getId());
        dto.put("joinDate",gamePlayers.getJoinDate());
        dto.put("player",makePlayerDTO(gamePlayers.getPlayerid()));


        return dto;
    }



    private List<Map> getHits(GamePlayer self,
                              GamePlayer opponent){
        List<Map> dto = new ArrayList<>();
        int carrier = 0;
        int destroyer = 0;
        int patrolboat = 0;
        int submarine= 0;
        int battleship = 0;

        for (Salvo salvo : opponent.getSalvos()) {
            Integer carrierHits = 0;
            Integer battleshipHits = 0;
            Integer submarineHits = 0;
            Integer destroyerHits = 0;
            Integer patrolboatHits = 0;
            Integer missedShots = salvo.getLocations().size();
            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();

            List<String> hitCellsList = new ArrayList<>();

            for (String salvoShot : salvo.getLocations()) {

                for (Ship ship : self.getShips()) {
                    for (String shiplocation : ship.getLocations()) {

                        if (shiplocation.contains(salvoShot)) {
                            if (ship.getType().equals("destroyer")) {
                                destroyer++;
                                destroyerHits++;
                                hitCellsList.add(salvoShot);
                                missedShots--;
                            } else if (ship.getType().equals("carrier")) {
                                carrier++;
                                carrierHits++;
                                hitCellsList.add(salvoShot);
                                missedShots--;
                            } else if (ship.getType().equals("submarine")) {
                                submarine++;
                                submarineHits++;
                                hitCellsList.add(salvoShot);
                                missedShots--;
                            } else if (ship.getType().equals("patrolboat")) {
                                patrolboat++;
                                patrolboatHits++;
                                hitCellsList.add(salvoShot);
                                missedShots--;
                            } else if (ship.getType().equals("battleship")) {
                                battleship++;
                                battleshipHits++;
                                hitCellsList.add(salvoShot);
                                missedShots--;

                            }

                        }

                    }
                }
            }
            damagesPerTurn.put("carrierHits", carrierHits);
            damagesPerTurn.put("battleshipHits", battleshipHits);
            damagesPerTurn.put("submarineHits", submarineHits);
            damagesPerTurn.put("destroyerHits", destroyerHits);
            damagesPerTurn.put("patrolboatHits", patrolboatHits);
            damagesPerTurn.put("carrier", carrier);
            damagesPerTurn.put("battleship", battleship);
            damagesPerTurn.put("submarine", submarine);
            damagesPerTurn.put("destroyer", destroyer);
            damagesPerTurn.put("patrolboat", patrolboat);
            hitsMapPerTurn.put("turn", salvo.getTurn());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);
            dto.add(hitsMapPerTurn);
        }
        return dto;
    }


    /**
     * verifica el estado del juego ( ganado, empatado, perdido, esperando a rival, esperé o juegue). lo devuelve en un map
     * @param self gameplayer propio
     * @param opponent gameplayer ajeno
     * @return devuelve map con los distintos etados
     */

    private String getGameState(GamePlayer self, GamePlayer opponent) {
        Set<Ship> selfShips = self.getShips();
        Set<Salvo> selfSalvoes = self.getSalvos();
        // si no puso barcos el jugador, que ponga barcos
        if (selfShips.size() == 0){
            return "PLACESHIPS";
        }
        // si oponente no inciializo el juego, que esperaa q arranca
        if (opponent.getShips() == null || opponent.equals(null)){
            return "WAITINGFOROPP";
        }

        long turn = getCurrentTurn(self, opponent);
        Set<Ship> opponentShips = opponent.getShips();
        Set<Salvo> opponentSalvoes = opponent.getSalvos();
        //si el oponente no puso barcos pero sí inicializo el juego, que espere
        if (opponentShips.size() == 0){
            return "WAIT";
        }
        //si ya termino el juego, los salvoes son iguales. verificamos quién hundió todo, el lo hundió, ganó.
        if(selfSalvoes.size() == opponentSalvoes.size()){
            Player selfPlayer = self.getPlayerid();
            Game game = self.getGameid();
            if (checkAllSunk(selfShips, opponentSalvoes) && checkAllSunk(opponentShips, selfSalvoes)){
                Score score = new Score(game, selfPlayer, 0.5f, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "TIE";
            }
            if (checkAllSunk(selfShips, opponentSalvoes)){
                Score score = new Score(game, selfPlayer, 0, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "LOST";
            }
            if(checkAllSunk(opponentShips, selfSalvoes)){
                Score score = new Score(game, selfPlayer, 1, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "WON";
            }
        }//si no tienen igual cantidad de turnos, verificamos que los disparos sean distinto de los turnos.
        // si lo son, que juguen, si no, que espere a que esten a la par para que arranque la verificacion
        // de vuelta

        if (selfSalvoes.size() != turn){
            return "PLAY";
        }
        return "WAIT";
    }


    /**
     * Metodo medio raro que tiro matias para medir la cantidad de salvoes disparados,  y de ahí sacar el turno.
     * @param self
     * @param opponent
     * @return
     */
    private long getCurrentTurn(GamePlayer self, GamePlayer opponent){
        if(self.getSalvos().size()==opponent.getSalvos().size()){
        return self.getSalvos().size();
        }else return Integer.max(self.getSalvos().size(),opponent.getSalvos().size());
    }

    /**
     * busca mediante for los gameplayers del game, y devuelve oponente
     * @param gameplayer a buscar su rival
     * @return gameplayer oponente
     */
    public GamePlayer opponentGamePlayer(GamePlayer gameplayer){

        Game game = gameplayer.getGameid();
        Set<GamePlayer> gamePlayers = game.getGamePlayers();
        for (GamePlayer gamePlayer: gamePlayers) {
            if(!gamePlayer.equals(gameplayer)){
                return gamePlayer;
            }
        }

        return gameplayer;
    }

    /**
     * Verifica existencia del
      * @param score
     * @param game
     * @return
     */
    private boolean existScore(Score score, Game game){
            Set<Score> scores = game.getScores();
            for(Score s: scores) {
                if(score.getPlayer().getEmail().equals(s.getPlayer().getEmail())){
                    return true;

                }

            }
            return false;

    }


    private int numberGamePlayers(){
        int i = 0;
        for (GamePlayer gamePlayer: gameplayerrepository.findAll()) {
            i++;
        }
        return i;
    }

    boolean checkAllSunk(Set<Ship> selfShip, Set<Salvo> oppSalvoes){
        boolean check = false;
        int sunklocationship = 0;
        int sunksship = 0;
        for(Ship ship: selfShip){

            for(String shiplocation: ship.getLocations()) {
                for (Salvo salvo : oppSalvoes) {
                    for (String salvolocation : salvo.getLocations()) {

                        if (shiplocation.equals(salvolocation)) {
                            sunklocationship++;
                        }
                    }
                }
            }
            if(ship.getLocations().size() == sunklocationship){
                sunksship++;
            }
            sunklocationship = 0;
        }
        if(sunksship == 5){
            check = true;
            return check;
        } else {
            check = false;
            return check;}
    }

    public Map<String, Object> getGameView(@PathVariable long id){
       return gameViewDTO(gameplayerrepository.findById(id).get());
     }

    private Map<String, Object> gameViewDTO(GamePlayer gamePlayer){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id",gamePlayer.getGameid().getId());
        dto.put("created", gamePlayer.getGameid().getCreationDate());
        dto.put("gamePlayers",getGamePlayerList(gamePlayer.getGameid().getGamePlayers()));
        dto.put("ships",gamePlayer.getShips());
        dto.put("salvoes", getSalvoList(gamePlayer.getGameid()));
        return dto;

    }

    private List<Map<String,Object>> getSalvoList(Game game){
        List<Map<String,Object>> myList = new ArrayList<>();
        game.getGamePlayers().forEach(gamePlayer -> myList.addAll(makeSalvoList(gamePlayer.getSalvos())));
        return myList;
    }


    public List<Map<String,Object>> makeSalvoList(Set<Salvo> salvos){

        return salvos.stream().map(salvo -> makeSalvoDTO(salvo)).collect(toList());

    }

    private Map<String, Object> makeSalvoDTO(Salvo salvo) {

        Map<String,Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id",salvo.getId());
        dto.put("turn", salvo.getTurn());
        dto.put("player",salvo.getGameplayer().getPlayerid().getId());
        dto.put("locations",salvo.getLocations());


        return dto;

    }


    /**
     * chequea si el usuario está logueado, y devuelve un map con null si no hay auth player, si no, devuelve dto del player
     * @param authentication objeto del usuario autenticado
     * @return mapa
     */

    @RequestMapping("/games")
    public Map<String,Object> checkLoggedPlayer (Authentication authentication){
         Map<String,Object> dto = new LinkedHashMap<>();
         authentication = SecurityContextHolder.getContext().getAuthentication();
         Player authenticatedPlayer = getAuthentication(authentication);
         if  (authenticatedPlayer == null)
             dto.put("player", "Guest");
         else
             dto.put("player", loggedPlayerDTO(authenticatedPlayer));
            dto.put("games", getAllgame());
                    return dto;


    }



    private Player getAuthentication(Authentication authentication ) {

        if (authentication == null || authentication instanceof AnonymousAuthenticationToken){
            return null;
    } else {
            return (playerRepository.findByEmail(authentication.getName()));
        }
    }

    private Map<String, Object> loggedPlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id",player.getId());
        dto.put("name",player.getEmail());

        return dto;
    }


    private List<Map<String, Object>> getGamePlayerList( Set<GamePlayer> gamePlayers ) {
        return  gamePlayers.stream().map(gamePlayer ->makeGameplayerDTO(gamePlayer)).collect(toList());

    }



    public List<Object> getAllgame() {
        return gameRepository.findAll().stream().map(game -> makeGameDTO(game)).collect(toList());

    }

    /**
     * hace un  DTO de un juego, que incluye DTO para gamaplayers luego usado en el games.html
     * @param game
     * @return
     */
    private Map<String, Object> makeGameDTO(Game game) {

       Map<String,Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", game.getId());
        dto.put("created", game.getCreationDate().getTime());
        dto.put("gamePlayers", game.getGamePlayers().stream()
                .map(gamePlayer -> makeGameplayerDTO(gamePlayer)).collect(toList()));
        dto.put("scores", getScoresList(game.getScores()));

        return dto;

    }

    /**
     * agarra unlistado de puntajes de un jugador, y luego lo convierte en lista de un DTO para cada puntaje
     * @param scores lista de puntajes a evaluar
     * @return lista de mapa de DTO para cada puntaje existente
     */

    private List<Map<String, Object>> getScoresList(Set<Score> scores ) {
        return  scores.stream().map(score -> ScoreDTO(score)).collect(toList());

    }

    private Map<String, Object> ScoreDTO(Score score) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("playerID",score.getPlayer().getId());
         dto.put("name", score.getPlayer().getEmail());
        dto.put("score",score.getScore());

        return dto;
    }

    /**
     * Hace Gameplayer DTO para luego ser invocado via Jscript
     * @param gamePlayer a hacerle el DTO
     * @return DTO del mismo.
     */

    private Map<String, Object> makeGameplayerDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("gpid", gamePlayer.getId());
        dto.put("joinDate",gamePlayer.getJoinDate().getTime());
        dto.put("player",makePlayerDTO(gamePlayer.getPlayerid()));
        dto.put("ships",gamePlayer.getShips());
        dto.put("salvoes",gamePlayer.getSalvos());

        return dto;
    }


    /**
     * hace un dto del jugador, util para loguear
     * @param player jugador logueado
     * @return dto con los datos ID y mail del jugador, para la web.
     */

    private Map<String, Object> makePlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("email",player.getEmail());



        return dto;
    }


    /**
     *
     * @return Genera leaderboard api
     */
    @RequestMapping("/leaderBoard")
    public List<Map<String,Object>> getallplayers() {
        return playerRepository.findAll().stream().map(player -> PlayerDTO(player)).collect(toList());

    }


    /**
     *
     * @param player:
     *
     * @return returns DTO con el ID y data del score del mismo. Sirve para el leaderboard
     */
    private Map<String, Object> PlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("score",makeScoresList(player));

        return dto;
    }

    /**
     * genera un DTO para el leaderboard
     * @param player al cual se hace la descripcion
     * @return el DTO de ese jugador
     */
    private Map<String, Object> makeScoresList(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("name", player.getEmail());
        dto.put("total",player.getScore(player));
        dto.put("won",player.getWin(player));
        dto.put("lost",player.getLost(player));
        dto.put("tied",player.getTied(player));
        return dto;
    }
//-------------------------------------------------------------------/*
    //Metodos auxiliares

    /**
     * hace un map que ya tiene coimo key "Error :". ahorra teclado... sobre todo para httpprequests.
     * @param string msg  a incluir en el error
     * @return mapa con en el key
     */
    private Map<String,Object> makeErrorMap(String string){
        Map<String,Object> auxMap = new LinkedHashMap<>();
        auxMap.put("Error", string);
        return auxMap;
    }
    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }




}
